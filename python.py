# Printing anything
print("Welcome to world of Python")

# Declaring an integer
var1 = 1
print(var1)

# Declaring a string
var2 = "Hello"
print(var2)

# Strings can also be declared using single quotes
var3 = 'World'
print(var3)

# Interpolating variables
var4 = f"{var2} {var3}"
print(var4)

# Using booleans
var5 = True
print(var5)
var6 = False
print(var6)

# Using float
var7 = 3.14
print(var7)

# Empty value
empty_var = None

# Adding numbers
var8 = var1 + var7
print(var8)

# Subtracting numbers
var9 = var1 - var7
print(var9)

# Multipling numbers
var10 = var1 * var7
print(var10)

# Dividing numbers
var11 = var1 / var7
print(var11)

# Declaring a list
var12 = [2, 'four', 6, 6.0, 'eight', 10.10, True, False, None]
print(var2)

# Lenght of a list
print(len(var12))

# First element of a list
print(var12[0])

# Last element of a list
print(var12[-1])

# Accessing any other index
var12[3] = 6.0
print(var12)

# Looping through a list
for element in var12:
    print(element)

# Using range operator
for i in range(0, len(var12)):
    print(var12[0])

# Type of a variable
for element in var12:
    print(f"{type(element)}")

# Continuing to next element of list
for element in var12:
    if type(element) == str:
        continue
    print(element)

# Breaking the loop
for element in var12:
    if type(element) == bool:
        break
    print(element)

# While loops
num = 10
while num > 2:
    print(num)
    num -= 2

# Else block in loops 
for elem in var12:
    print(elem)
else:
    print("Looped through all the elements.")

for elem in var12:
    if elem == None:
        break
else:
   print("Oops! couldn't loop through all the elements.")

# Adding an element to a list from behind
var12.append('foo')

# Adding an element to a list from front
var12.insert(0, 'zero')

# Inserting an element at any index
var12.insert(2, 3)
print(var12)

# Combine two lists
my_list = [1, 2, 3]
my_list.extend([4, 5, 6])  # my_list is now [1, 2, 3, 4, 5, 6]

# Remove the first occurrence of a specified item from the list.
my_list = [1, 2, 3, 2]
my_list.remove(2)  # my_list is now [1, 3, 2] 

# Removes and returns the item at the given index (defaults to the last item). Raises an IndexError if the list is empty or the index is out of range.
my_list = [1, 2, 3]
last_item = my_list.pop(1)  # my_list is now [1, 3]
last_item = my_list.pop()  # my_list is now [1] last_item is 3

# Remove all items from the list.
my_list = [1, 2, 3] 
my_list.clear()  # my_list is now []

# Returns the index of the first occurrence of the specified item. 
my_list = [1, 2, 3, 2]
index_of_2 = my_list.index(2)  # index_of_2 is 1. Raises a ValueError if not found.

# Returns the number of times an item appears in the list.
my_list = [1, 2, 2, 3]
num_twos = my_list.count(2)   # num_twos is 2

# Sort the items of the list in ascending order 
my_list = [3, 1, 5, 2]
my_list.sort()  # my_list is now [1, 2, 3, 5] (can take a reverse=True argument for descending order).

# Reverse the order of the items in the list in-place.
my_list = [1, 2, 3]
my_list.reverse()  # my_list is now [3, 2, 1]

# Create a new copy of the list.
new_list = [1, 2, 3].copy()


# Copied from: https://g.co/bard/share/2e528a519b91

# Declaring a dictionary directly
my_dict = {"name": "Alice", "age": 30, "city": "New York"}

# Declaring a dictionary using dict function
my_dict = dict(name="Bob", age=25, city="London")

# Accessing the element value directly using the element key
name = my_dict["name"]  # Returns "Bob"

# Accessing the element value using the get method
age = my_dict.get("age")  # Returns 25
occupation = my_dict.get("occupation", "Unknown")  # Returns "Unknown" if "occupation" key doesn't exist

# Adding or updating items
my_dict["email"] = "bob@example.com"
my_dict["age"] = 31  # Updates the existing "age" value

# Update multiple items of the dictionary
my_dict.update({"occupation": "Developer", "hobbies": ["coding", "reading"]})


# Removing items using pop method
removed_item = my_dict.pop("city")

# Removing items using del method
del my_dict["email"]

# Iterating over keys
for key in my_dict:
    print(key, my_dict[key])

for key in my_dict.keys():
    print(key)

# Iterating over values
for value in my_dict.values():
    print(value)

# Iterating over key-value pairs
for key, value in my_dict.items():
    print(key, value)

# Create a copy of dictionary
new_dict = my_dict.copy()

# Clear all the elements of the dictionary
new_dict.clear()
print(new_dict)
print(my_dict)


# Declaring a set


# Python regular expression explanation and examples:

# https://g.co/bard/share/6947dab2637f

# https://g.co/bard/share/54ab71961845




